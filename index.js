// Use the "require" directive to load the HTTP module of node JS
// A 'module' is a software component or part of a program that contains one or more routines
// The 'http module' let's Node.js transfer data using the HTTP(Hypertext Transfer Protocol).
// HTTP is a protocol that allows the fetching of resoursces such as HTML documents
// Clients (browser) and servers(Node.JS/Express.JS applications) communicate exchanging individual messagess

// Request - messages sent by the client (usally in a web browser)
// Responses - messages sent by the server as an answer to the client

let http = require("http");
// const route = require("./routes.js");

// CREATE A SERVER
/**
 * - The http module has a createServer() method that accepts a function as an argument and allows server creation.
 * - The arguments passed in the function are request & response objects (data type) that contain methods that allow us to receive requests from the client and send the responses back.
 * - Using the module's createServer() method, we can create an HTTP server that listens to the requests on a specified port and gives back to the client.
 */

// Define the Port Number that the server will be listening to

http
  .createServer((req, res) => {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Hello Roda");
  })
  .listen(4000);

console.log("Server running at localhost:4000");

// A port is virtual point where network connections start and end

// Send a response back to the client
// Use the writeHead() method
// Set a status code for the response - a 200 - OK

// Inputting the command tells our device to run node js
// node index.js

// Install nodemon via terminal
// npm install -g nodemon

// To check if nodemon is already installed via terminal
// nodemon -v

/**
 * Important Note:
 * - Installing the package will allow the server to automatically restart when files have been changed or updated
 */
