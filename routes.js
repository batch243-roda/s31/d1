/** CREATE A ROUTE
 *  "/greeting"
 *
 *  -
 */
const http = require("http");
//
const port = 4000;

const server = http.createServer((req, res) => {
  // Accessing the 'greeting' route returns a message of 'Hello World'
  if (req.url == "/greeting") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Hello Greeting");
  } else if (req.url == "/home") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Hello Home");
  } else {
    res.writeHead(404, { "Content-Type": "html" });
    res.end("<h1>Page Not Found!</h1>");
  }
});

server.listen(port);

console.log(`Listening to port: ${port}`);
